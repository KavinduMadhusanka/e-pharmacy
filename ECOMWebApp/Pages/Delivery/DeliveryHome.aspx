﻿<%@ Page Title="Delivery" Language="C#" MasterPageFile="~/Pages/Shared/DeliveryMaster.Master" AutoEventWireup="true" CodeBehind="DeliveryHome.aspx.cs" Inherits="ECOMWebApp.Pages.Delivery.DeliveryHome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">

     <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link rel="stylesheet" href="../../Scripts/CSS3Clock/calenderap.css" />--%>

    <link href="../../Scripts/vanilla-calendar-master/dist/vanillaCalendar.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../Scripts/Bootstrap-style-Datetime-Picker-Plugin/dist/jquery.datetimepicker.css"/>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphForm" runat="server">

     <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-6">
                    <h1 class="m-0">Delivery Dashboard</h1>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">

    <script type="text/javascript" src="../../Scripts/CSS3Clock/jquery-1.2.6.min.js"></script>
    
    
</asp:Content>
